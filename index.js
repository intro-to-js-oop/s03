//alert("test");


//Class blueprint
//In JS, classes can be created using the "class" keyword and {}
//Naming convention for classes: Begin with Uppercase characters

/*
	Syntax:

		class <Name> {
			
		}

*/


//here we have an empty student class
/*class Student {
	constructor(name, email, grade){
		//propertyName = value;
		this.name = name;
		this.email = email;
		this.grade = grade;
	}
}*/

//argument, new -> constructor
//Instantiation - process of creating objects from class
//To create an object from a class use the "new" keyword, when a class has a contructor, we need to supply all the values needed by the constructor
/*
let studentOne = new Student('john', 'john@mail.com');
console.log(studentOne);
*/

/*
	Mini - Exercise:
		Create a new class called Person

		This Person class should be able to instantiate a new object with the ff fields:

		name,
		age, (should be a number and must be a greater than or equal to 18, otherwise, set the property to undefined)
		nationality,
		address

		Instantiate 2 new objects from the Person class as person1 and person2

		Log both objects in the console. Take a screenshot of your console and send it to our group chat
*/


class Person {
	constructor(name, age, nationality, address){
		this.name = name;
		if(typeof age === 'number' && age >= 18){
			this.age = age ;
		}
		else {
			this.age = undefined;
		}
		this.nationality = nationality;
		this.address = address;
	}
}

let person1 = new Person('Jack', 18, "Fil", "PH");
let person2 = new Person('Jake', 11, "Fil", "PH");

console.log(person1);
console.log(person2);




//Activity 1

/*
1. Class
2. PascalCase
3. new
4. instantiate
5. constructor method
*/


class Student {
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		this.grades = grades;
		for(let grade = 0; grade < grades.length; grade ++){
			if(grades[grade] > 100 || grades[grade] < 0 || typeof grades[grade] == 'string' || grades.length == 3){
				this.grades = undefined;
			}
		}
	}

	//array every

	//studentFour.login().listGrades().logout()

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	
	logout() {
		console.log(`${this.email} has logged out`);
		return this;
	}

	listGrades() {
			console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
			return this;
		}

	computeAve(){
		let sum = 0;
		this.grades.forEach(x => {
			sum += x;
		});
		this.gradeAve = sum/4
		return this;
	}

	willPass() {
		this.passed = this.gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		if(this.passed) {
			if(this.gradeAve >= 90){
				this.passedWithHonors = true;
				return this;
			}
			else {
				this.passedWithHonors = false;
				return this;
			}	
		}
		else {
			return this;
		}
	}
}



let studentOne = new Student('john', 'john@mail.com', [89, 84, 80, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

console.log(studentOne.login().computeAve().willPass().willPassWithHonors().logout());

console.log(studentTwo.login().computeAve().willPass().willPassWithHonors().logout());
console.log(studentThree.login().computeAve().willPass().willPassWithHonors().logout());
console.log(studentFour.login().computeAve().willPass().willPassWithHonors().logout());
//getter and setter
//Best practice dictates that we regulate access to such properties. We do so, via the use of "getters"(regulates retrieval) and setter (regulates manipulation)

//Method Chaining



//Activity 2

/*
1. properties
2. False / no
3. True / yes
4. Getter and Setter
5. this

*/


